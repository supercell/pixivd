module pixivd.enums;

enum ContentType
{
    illustration = 0,
    manga = 1,
    ugoira = 2,
    novel
}

/**
 * The mode for downloading daily illustrations.
 */
enum DailyMode : string {
    all = "all",
    r18 = "r18",
    safe = "safe",
}
