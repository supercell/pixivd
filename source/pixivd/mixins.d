module pixivd.mixins;

package(pixivd) template mixCheckJsonError(string jsonVarName)
{
    static if (__VERSION__ < 2083L)
    {
        const char[] mixCheckJsonError = jsonVarName ~ "[\"error\"].type == JSON_TYPE.TRUE";
    }
    else
    {
        const char[] mixCheckJsonError = jsonVarName ~ "[\"error\"].boolean == true";
    }
}

package(pixivd) template mixGetJsonBoolean(string jsonVarName, string key) {
    static if (__VERSION__ < 2083L)
    {
        const char[] mixGetJsonBoolean = jsonVarName ~ "[\"" ~ key ~ "\"].type == JSON_TYPE.TRUE";
    }
    else
    {
        const char[] mixGetJsonBoolean = jsonVarName ~ "[\"" ~ key ~ "\"].boolean()";
    }
}
