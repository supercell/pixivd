module pixivd.client;

import std.array : appender;
import std.file : FileException, rename;
import std.format : format;
import std.json;
import std.net.curl : HTTP;
import std.signals;

import graphicsmagick_c;
import graphicsmagick_c.magick.api;

import pixivd.enums;
import pixivd.mixins;
import pixivd.types;

public enum PixivDVersion = 0.8;
public enum PixivDVersionString = "0.8";

enum kUserAgent = "Mozilla/5.0 (Windows NT 10.0; rv:108.0) Gecko/20100101 Firefox/108.0";

struct DownloadProgressEvent
{
    size_t total;
    size_t current;
}

///
/// Signals that a download has finished.  This is used for
/// Illustrations, Manga, and Ugoira.
///
/// The `forPage` member indicates that this signal is for
/// an individual page of the Illustration or Manga only.
///
struct DownloadCompleteEvent
{
    /// Is the download for an individual page?
    const bool forPage = false;
}

///
/// Signals that a page in the Illustration has already
/// been downloaded and will not be downloaded again.
///
struct PageAlreadyDownloadedEvent
{
    /// The current page number (1-based numbering)
    size_t currentPage;
    /// The total number of pages (1-based numbering)
    size_t totalPages;
}

/**
 *
 * The main client for performing pixiv API requests.
 *
 * ```
 * // Create without setting the PHPSESSID
 * auto client = new Client();
 * // Set the PHPSESSID
 * client.phpsessid = "COOKIE_VALUE_HERE";
 * ```
 */
class Client
{
private:

    string m_phpsessid;

    HTTP m_client;

    bool m_isGMInitialized = false;

public:

    /**
     * Return the PHPSESSID that is being used.
     */
    @property string phpsessid() const
    {
        return m_phpsessid;
    }

    /**
     * Set a new PHPSESSID to be used for new requests.
     */
    @property void phpsessid(string sessionID)
    {
        m_phpsessid = sessionID;
        m_client.setCookie("PHPSESSID=" ~ m_phpsessid);
    }

    /**
     * Create a new instance of Client, providing a PHPSESSID to begin
     * with.
     *
     * Params:
     *  phpsessid = The PHPSESSID Cookie from a Web Browser.
     */
    this(string phpsessid)
    {
        m_phpsessid = phpsessid;
        m_client = HTTP();
        this._resetHeaders();
    }

    /**
     * Create a new instance of Client.
     *
     * This will allow you to interact with the pixiv API without an
     * account.  However, your access will be limited to what is
     * publically available.
     *
     * Use `phpsessid(string)` to set a PHPSESSID, meaning you will
     * be logged in.
     */
    this()
    {
        this("");
    }

    ~this()
    {
        m_client.shutdown();
        if (true == m_isGMInitialized) {
            DestroyMagick();
	    }
    }

    /* Signals */
    mixin Signal!(DownloadProgressEvent);
    mixin Signal!(DownloadCompleteEvent);
    mixin Signal!(PageAlreadyDownloadedEvent);

    /**
     * Fetch an illustration
     *
     * Throws:
     *  - ConvException if we failed to parse the `id` as a string.
     *  - PixivExcetpion if something happened when parsing the response.
     */
    Illustration fetchIllustration(string id)
    {
        auto response = appender!string;

        m_client.url = format!"https://www.pixiv.net/ajax/illust/%s"(id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };

        m_client.perform();

        JSONValue json = parseJSON(response.data());

        return Illustration.fromJSON(json);
    }

    /** 
     * Fetch an illustration.
     *
     * Params:
     *   thumb = A Thumbnail for the image you want to fetch.
     * Returns: A complete Image instance for the provided Thumbnail.
     */
    Illustration fetchIllustration(Thumbnail thumb)
    {
        auto response = appender!string;

        m_client.url = format!"https://www.pixiv.net/ajax/illust/%s"(thumb.id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };

        m_client.perform();

        JSONValue json = parseJSON(response.data());

        return Illustration.fromJSON(json);
    }

    ///
    unittest
    {
        import core.thread : Thread;
        import core.time : seconds;

        import std.net.curl : CurlTimeoutException;
        import std.stdio : writefln;

        auto client = new Client();

        try
        {
            Illustration illust = client.fetchIllustration("95917058");
            assert("95917058" == illust.id);
            assert("user_hccs8584" == illust.userAccount);

            writefln("%s -- Sleeping for 3 seconds", __PRETTY_FUNCTION__);
            Thread.sleep(3.seconds); // Try not to abuse the server.
        }
        catch (CurlTimeoutException cte)
        {
            // It's possible that there is no internet connection
            // or that Pixiv is down, so we won't error.
            writefln("CurlTimeoutException: %s", cte.msg);
        }
    }

    /// ditto
    Illustration fetchIllustration(size_t id)
    {
        import std.conv : to;

        return fetchIllustration(to!string(id));
    }

    ///
    unittest
    {
        import core.thread : Thread;
        import core.time : seconds;

        import std.net.curl : CurlTimeoutException;
        import std.stdio : writefln;

        auto client = new Client();

        try
        {
            Illustration illust = client.fetchIllustration(95917058);
            assert("95917058" == illust.id);
            assert("user_hccs8584" == illust.userAccount);

            writefln("%s -- Sleeping for 3 seconds", __PRETTY_FUNCTION__);
            Thread.sleep(3.seconds); // Try not to abuse the server.
        }
        catch (CurlTimeoutException cte)
        {
            // It's possible that there is no internet connection
            // or that Pixiv is down, so we won't error.
            writefln("CurlTimeoutException: %s", cte.msg);
        }
    }

    Thumbnail[] fetchIllustrationsFollowing(long page = 1, DailyMode mode = DailyMode.all, string lang = "en")
    {
        auto res = appender!string;

        m_client.url = format!"https://www.pixiv.net/ajax/follow_latest/illust?p=%d&mode=%s&lang=%s"(page, mode, lang);
        m_client.onReceive = (ubyte[] data) { res.put(data); return data.length; };

        m_client.perform();

        JSONValue json = parseJSON(res.data());

        static if (__VERSION__ < 2083L)
        {
            if (JSON_TYPE.TRUE == json["error"].type)
            {
                throw new PixivJSONException(json["message"].str);
            }
        }
        else
        {
            if (true == json["error"].boolean)
            {
                throw new PixivJSONException(json["message"].str);
            }
        }

        if ("body" !in json)
        {
            throw new PixivJSONException("No \"body\" in returned JSON.");
        }

        auto body_ = json["body"];

        if ("thumbnails" !in body_)
        {
            throw new PixivJSONException("No \"thumbnails\" in fetchIllustrationsFollowing");
        }

        Thumbnail[] thumbnails;
        auto jsonThumbs = body_["thumbnails"]["illust"].array;

        foreach (ref thumb; jsonThumbs)
        {
            thumbnails ~= Thumbnail.fromJSON(thumb);
        }

        return thumbnails;
    }

    ///
    unittest
    {
        import std.net.curl : CurlTimeoutException;
        import std.process : environment;
        import std.stdio : writefln;

        auto phpsessid = environment.get("PIXIV_PHPSESSID");
        if (null is phpsessid)
        {
            // Won't test as there is no PHPSESSID.
            return;
        }

        auto client = new Client(phpsessid);

        try
        {
            import core.thread : Thread;
            import core.time : seconds;

            Thumbnail[] thumbs = client.fetchIllustrationsFollowing();
            assert(thumbs.length > 0);

            writefln("%s -- Sleeping for 3 seconds.", __PRETTY_FUNCTION__);
            Thread.sleep(3.seconds);
        }
        catch (CurlTimeoutException cte)
        {
            // It's possible that there is no internet connection
            // or that Pixiv is down, so we won't error.
            writefln("CurlTimeoutException: %s", cte.msg);
        }
    }

    /** 
     * Fetch a list of Users that the logged in user follows.
     *
     * Params:
     *   offset = [in] The number of accounts to offset the retrieval by
     *   limit = [in] Limit the number of accounts returned
     *   rest = [in] Whether to retrieve public ("show"), or private ("hide") followings.
     * Returns: An array of `User`.
     */
    User[] fetchFollowing(int offset, int limit = 24, string rest = "show")
    {
        long numAccounts;
        return fetchFollowing(offset, numAccounts, limit, rest);
    }

    /** 
     * Fetch a list of Users that the logged in user follows.
     *
     * An optional `totalNumberOfAccounts` parameter allows you to store the total number
     * of accounts the user follows (the value differs depending on `rest`).
     *
     * Params:
     *   offset = The number of accounts to offset the retrieval by.
     *   totalNumberOfAccounts = [out] The total number of accounts the user follows (depends on `rest`).
     *   limit = Limit the number of accounts returned (default 24).
     *   rest = [in] Whether to retrieve public ("show"), or private ("hide") followings.
     */
    User[] fetchFollowing(in int offset, out long totalNumberOfAccounts, in int limit = 24, in string rest = "show")
    {
        import std.string : split;

        auto response = appender!string;

        if ("" == m_phpsessid)
        {
            throw new Exception("PHPSESSID not set. Please use `.phpsessid = `");
        }

        string id = m_phpsessid.split('_')[0];
        if ("" == id)
        {
            throw new Exception("Could not determine user id");
        }

        m_client.url = "https://www.pixiv.net/ajax/user/%s/following?offset=%d&limit=%d&rest=%s&lang=en".format(id,
            offset, limit, rest);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };
        m_client.perform();

        auto json = parseJSON(response[]);
        if (mixin(mixCheckJsonError!("json")))
        {
            throw new PixivJSONException(json["message"].str);
        }

        if ("body" !in json)
        {
            throw new PixivJSONException("No \"body\" in returned JSON.");
        }

        User[] users;
        totalNumberOfAccounts = json["body"]["total"].integer;
        auto jsonUsers = json["body"]["users"].array;
        foreach (ref jsonUser; jsonUsers)
        {
            users ~= User.fromJSON(jsonUser);
        }

        return users;
    }

    ///
    unittest
    {
        import std.net.curl : CurlTimeoutException;
        import std.process : environment;
        import std.stdio : writefln;

        auto sessid = environment.get("PIXIV_PHPSESSID");
        if (null is sessid)
        {
            // test requires authorisation, so will skip.
            return;
        }

        auto client = new Client(sessid);
        try
        {
            User[] users = client.fetchFollowing(0);
            assert(0 == users.length, "No users returned (assumes you follow anyone)");

            foreach (user; users)
            {
                assert("" != user.userId);
                assert("" != user.userName);
            }
        }
        catch (CurlTimeoutException cte)
        {
            // Possible that there is no internet connection
            // or that pixiv is down.
            writefln("CurlTimeoutException: %s", cte.msg);
        }
    }

    FullUser fetchUser(User user)
    {
        return fetchUser(user.userId);
    }

    FullUser fetchUser(string id)
    {
        auto response = appender!string;

        m_client.url = "https://www.pixiv.net/ajax/user/%s?full=1&lang=en".format(id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };

        m_client.perform();

        auto json = parseJSON(response.data());
        if (mixin(mixCheckJsonError!("json")))
        {
            throw new PixivJSONException(json["message"].str);
        }

        if ("body" !in json)
        {
            throw new PixivJSONException("JSON response contains no 'body'.");
        }

        return FullUser.fromJSON(json["body"]);
    }

    ///
    unittest
    {
        import std.net.curl : CurlTimeoutException;

        auto client = new Client();
        FullUser user;

        try
        {
            user = client.fetchUser("4938312");
        }
        catch (CurlTimeoutException cte)
        {
            import std.stdio : stderr;

            stderr.writefln("%s -- Failed to connect to server: ", __PRETTY_FUNCTION__, cte.msg);
            return; // exit test early.
        }
        assert("4938312" == user.userId, "user.userId != 4938312");
        assert(false == user.following, "user.following != false");
        assert(false == user.followed, "user.followed != false");
    }
    
    UserBrief fetchUserAll(string id)
    {
        auto response = appender!string;

        m_client.url = "https://www.pixiv.net/ajax/user/%s/profile/all".format(id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };

        m_client.perform();

        auto json = parseJSON(response.data());
        if (mixin(mixCheckJsonError!("json")))
        {
            throw new PixivJSONException(json["message"].str);
        }

        if ("body" !in json)
        {
            throw new PixivJSONException("JSON response contains no 'body'.");
        }

        return UserBrief.fromJSON(json["body"]);
    }

    /** 
     * Download an Illustration (incl. manga, novels, ugoira).
     *
     * Params:
     *   illust = Illustration to download
     *   directory = Directory where to save the Illustration
     *   overwrite = Overwrite existing file(s)?
     *
     * Throws:
     *  * `FileException` when the image file (or directory) already exists.
     *  * `PixivMagickException` on failure to dynamically load GraphicsMagick.
     *  * `PixivJSONException` on error parsing JSON or with API request.
     *  * `PixivException` on other Pixiv-related errors.
     *  * `Exception` if trying to download Novel (NotImplemented) or any other exception.
     * 
     */
    void downloadIllust(Illustration illust, string directory, bool overwrite = false)
    {
        switch (illust.type)
        {
        case ContentType.illustration:
            _downloadIllustration(illust, directory, overwrite);
            break;
        case ContentType.manga:
            _downloadManga(illust, directory, overwrite);
            break;
        case ContentType.novel:
            _downloadNovel(illust, directory, overwrite);
            break;
        case ContentType.ugoira:
            _downloadUgoira(illust, directory, overwrite);
            break;
        default:
            assert(0, "Unsupported Content Type for illustration " ~ illust.id);
        }
    }

    ///
    unittest
    {
        import std.datetime.systime : SysTime;
        import std.file : exists, getcwd, getTimes, remove;
        import std.path : extension;
        import std.stdio : File;

        auto client = new Client();

        try
        {
            // Download a single illustration
            Illustration illust = client.fetchIllustration("82631500");
            assert("フフフ" == illust.title, "Incorrect illustration title");
            assert(1 == illust.pages, "Incorrect number of pages");
            assert("2020-06-28T15:13:00+00:00" == illust.createDate,
                "Incorrect createDate (got " ~ illust.createDate ~ ")");

            const fileName = illust.id ~ illust.urls["original"].extension;

            client.downloadIllust(illust, getcwd(), true);

            assert(true == exists(fileName), "true != exists(fileName)");
            File imageFile = File(fileName, "r");
            scope (exit)
            {
                remove(fileName);
            }

            SysTime createDate = SysTime.fromISOExtString(illust.createDate);
            SysTime oAccessTime;
            SysTime oModificationTime;
            getTimes(fileName, oAccessTime, oModificationTime);

            assert(oAccessTime == createDate, "oAccessTime != createDate");
            assert(oModificationTime == createDate, "oModificationTIme != createDate");
            assert(imageFile.size > 0, "illust file size is not 0");
        }
        catch (Exception e)
        {
            assert(false, e.msg);
        }

        import std.stdio : stderr;
        import core.thread : Thread;
        import core.time : seconds;

        stderr.writefln("%s -- Sleeping for 3 seconds", __PRETTY_FUNCTION__);
        Thread.sleep(3.seconds);
    }

    ///
    unittest
    {
        import std.file : chdir, exists, getcwd, rmdirRecurse;
        import std.format : format;
        import std.net.curl : CurlTimeoutException;
        import std.path : extension;

        auto client = new Client();

        try
        {
            Illustration illust = client.fetchIllustration("81573844");

            assert("過去絵ミクさん" == illust.title, "Incorrect illustration title");
            assert(2 == illust.pages, "Incorrect number of pages");

            client.downloadIllust(illust, getcwd(), true);
            const ext = extension(illust.urls["original"]);

            chdir(illust.id);

            foreach (i; 0 .. illust.pages)
            {
                const filename = format!"%s_p%d%s"(illust.id, i, ext);
                assert(exists(filename), "Multi-paged image not downloading all pages");
            }

            chdir("..");

            rmdirRecurse(illust.id);
        }
        catch (CurlTimeoutException cte)
        {
            // PASS
            import std.stdio : stderr;

            stderr.writefln("TIMEOUT: %s", cte.msg);
        }

        import std.stdio : stderr;
        import core.thread : Thread;
        import core.time : seconds;

        stderr.writefln("%s -- Sleeping for 3 seconds", __PRETTY_FUNCTION__);
        Thread.sleep(3.seconds);
    }

    /** 
     * Fetch metadata for an ugoira (moving illustration).
     *
     * Params:
     *   illust = The illustration (which is an Ugoira) to fetch the metadata for
     * Returns: An Ugoira instance containing the source URL and Frame durations for the ugoira.
     */
    Ugoira fetchUgoiraMeta(Illustration illust)
    {
        return fetchUgoiraMeta(illust.id);
    }

    /** 
     * Fetch metadata for an ugoira (moving illustration).
     *
     * Params:
     *   id = The illustration id to fetch the metadata for.
     * Returns: An Ugoira instance containing the source URL and Frame durations for the ugoira.
     */
    Ugoira fetchUgoiraMeta(string id)
    {
        auto response = appender!string;

        m_client.url = format!"https://www.pixiv.net/ajax/illust/%s/ugoira_meta"(id);
        m_client.onReceive = (ubyte[] data) {
            response ~= data;
            return data.length;
        };
        m_client.perform();

        JSONValue json = parseJSON(response.data());

        if ("body" !in json)
        {
            throw new PixivJSONException("No \"body\" in JSON response.");
        }
        return Ugoira.fromJSON(json);
    }

    ///
    unittest
    {
        auto client = new Client();

        Ugoira ugoira = client.fetchUgoiraMeta("103331804");

        assert("image/jpeg" == ugoira.getMimeType());
        assert("https://i.pximg.net/img-zip-ugoira/img/2022/12/04/16/13/51/103331804_ugoira600x600.zip" ==
                ugoira.getSource());

        import std.stdio : stderr;
        import core.thread : Thread;
        import core.time : seconds;

        stderr.writefln("%s -- Sleeping for 3 seconds", __PRETTY_FUNCTION__);
        Thread.sleep(3.seconds);
    }

private:

    void _downloadIllustration(Illustration illust, string directory, bool overwrite)
    {
        import std.file : getcwd, chdir, mkdirRecurse, exists;

        if (false == exists(directory))
        {
            mkdirRecurse(directory);
        }

        const originalDir = getcwd();
        chdir(directory);

        if (1 == illust.pages)
        {
            _downloadSingleIllust(illust, overwrite);
        }
        else
        {
            _downloadPagedIllust(illust, overwrite);
        }

        chdir(originalDir);
    }

    void _downloadSingleIllust(Illustration illust, bool overwrite)
    {
        import std.datetime.systime;

        import std.file : exists, rename, setTimes;
        import std.path : extension;
        import std.stdio : File;

        const baseFileName = illust.id ~ illust.urls["original"].extension;
        const partFileName = baseFileName ~ ".part";
        string mode = "w+";

        if (true == exists(baseFileName) && false == overwrite)
        {
            throw new FileException(baseFileName, "File already exists");
        }
        if (true == exists(partFileName)) {
            this._setupPartialDownload(illust.urls["original"], partFileName);
            mode = "a+";
            this._resetHeaders();
        }

        File imageFile = File(partFileName, mode);

        m_client.url = illust.urls["original"];
        m_client.onReceive = (ubyte[] data) {
            imageFile.rawWrite(data);
            return data.length;
        };
        m_client.onProgress = (size_t dlTotal, size_t dlNow, size_t ulTotal, size_t ulNow) {
            emit(DownloadProgressEvent(dlTotal, dlNow));
            return 0;
        };

        m_client.perform();
        imageFile.close();
        rename(partFileName, baseFileName);
        emit(DownloadCompleteEvent());

        SysTime createDate = SysTime.fromISOExtString(illust.createDate);
        setTimes(baseFileName, createDate, createDate);

        this._resetHeaders();
    }

    void _downloadPagedIllust(Illustration illust, bool overwrite)
    {
        import std.array : appender;
        import std.file : chdir, exists, getcwd, mkdir;
        import std.stdio : File;

        if (false == exists(illust.id))
        {
            mkdir(illust.id);
        }
        const originalDir = getcwd();
        chdir(illust.id);
        scope (exit)
            chdir(originalDir);

        auto response = appender!string;

        m_client.url = "https://www.pixiv.net/ajax/illust/%s/pages".format(illust.id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;
        };
        m_client.perform();

        JSONValue json = parseJSON(response.data());
        if (mixin(mixCheckJsonError!("json")))
        {
            throw new PixivJSONException(json["msg"].str);
        }

        if ("body" !in json)
        {
            throw new PixivJSONException("JSON response contains no 'body'.");
        }

        const jsonBody = json["body"].array;
        // +1 so this matches PageAlreadyDownloaded containing the
        // 1-based numbering of the current page.
        const totalNumberOfPages = jsonBody.length;

        foreach (pageNumber, jsonobj; jsonBody)
        {
            import std.path : baseName, stripExtension;

            const url = jsonobj["urls"]["original"].str;
            const baseFileName = baseName(url);
            const partFileName = baseFileName ~ ".part";
            string mode = "w+";

            if (true == exists(baseFileName) && false == overwrite) {
                emit(PageAlreadyDownloadedEvent(pageNumber + 1, totalNumberOfPages));
                continue;
            }

            if (true == exists(partFileName)) {
                this._setupPartialDownload(jsonobj["urls"]["original"].str, partFileName);
                mode = "a+";
                this._resetHeaders();
            }

            File img = File(partFileName, mode);

            m_client.url = url;
            m_client.onReceive = (ubyte[] data) {
                img.rawWrite(data);
                return data.length;
            };
            m_client.onProgress = (size_t dlTotal, size_t dlNow, size_t ulTotal, size_t ulNow) {
                emit(DownloadProgressEvent(dlTotal, dlNow));
                return 0;
            };


            m_client.perform();
            img.close();
            rename(partFileName, baseFileName);
            emit(DownloadCompleteEvent(true));
        }
        emit(DownloadCompleteEvent());
    }

    void _downloadManga(Illustration illust, string directory, bool overwrite)
    {
        import std.array : appender;
        import std.datetime : SysTime;
        import std.file : FileException, chdir, getcwd, exists, mkdirRecurse, setTimes;
        import std.stdio : File;
        import std.path : baseName, buildPath;

        if (false == exists(directory)) {
            mkdirRecurse(directory);
        }

        immutable origDir = getcwd();
        chdir(directory);
        scope(exit) chdir(origDir);

        if (false == exists(illust.id)) {
            mkdirRecurse(illust.id);
        }
        chdir(illust.id);

        auto response = appender!string;
        m_client.url = "https://www.pixiv.net/ajax/illust/%s/pages".format(illust.id);
        m_client.onReceive = (ubyte[] data) {
            response.put(data);
            return data.length;  
        };

        m_client.perform();

        auto json = parseJSON(response.data());
        if (mixin(mixCheckJsonError!("json"))){
            throw new PixivJSONException(json["message"].str);
        }

        auto bodyArr = json["body"].array;

        foreach(obj; bodyArr) {
            const baseFileName = baseName(obj["urls"]["original"].str);
            const partFileName = baseFileName ~ ".part";
            string mode = "w+";

            if (true == exists(baseFileName) && false == overwrite) {
                emit(PageAlreadyDownloadedEvent());
                continue;
            }
            if (true == exists(partFileName)) {
                this._setupPartialDownload(obj["urls"]["original"].str, partFileName);
                mode = "a+";
                this._resetHeaders();
            }

            File outFile = File(partFileName, mode);
            m_client.url = obj["urls"]["original"].str;
            m_client.onReceive = (ubyte[] data) {
                outFile.rawWrite(data);
                return data.length;
            };
            m_client.onProgress = (size_t dlTotal, size_t dlNow, size_t ulTotal, size_t ulNow) {
                emit(DownloadProgressEvent(dlTotal, dlNow));
                return 0;
            };
            m_client.perform();
            outFile.close();
            rename(partFileName, baseFileName);
            emit(DownloadCompleteEvent(true));

            SysTime createDate = SysTime.fromISOExtString(illust.createDate);

            setTimes(baseFileName, createDate, createDate);
        }
        emit(DownloadCompleteEvent());
    }

    void _downloadNovel(Illustration illust, string directory, bool overwrite)
    {
        throw new Exception("Unsupported media type: Novel");
    }

    void _downloadUgoira(Illustration illust, string directory, bool overwrite)
    {
        import core.stdc.string : strlen, strncpy;

        import std.file : chdir, getcwd, mkdirRecurse, read, remove, rmdirRecurse,
                          tempDir;
        import std.format : format;
        import std.path : baseName, buildPath;
        import std.stdio : File;
        import std.string : fromStringz, toStringz;
        import std.zip : ZipArchive, ArchiveMember;

        // We want to download a GIF.
        // To do this we:
        //  1. Download the zip file for the Ugoira
        //  2. Extract the zip file
        //  3. Create an ImageList in GraphicsMagick
        //  4. Append all the individual frames with the specific delay.
        //  5. Save the ImageList as a GIF file.

        // NOTE: There is no need to remove the individual files as all
        // work takes place in a directory which is deleted at the end.

        string gifFileName = illust.id ~ ".gif";

        string originalDir = getcwd();
        chdir(tempDir());

        mkdirRecurse(illust.id);
        chdir(illust.id);
        scope (exit)
        {
            // We will likely be in the |directory| provided.
            chdir(tempDir());
            rmdirRecurse(illust.id);

            chdir(originalDir);
        }

        Ugoira ugoira = fetchUgoiraMeta(illust);

        string zipFileName = baseName(ugoira.getOriginalSource());

        auto zipFile = File(zipFileName, "w+");

        m_client.url = ugoira.getOriginalSource();
        m_client.onReceive = (ubyte[] data) {
            zipFile.rawWrite(data);
            return data.length;
        };
        m_client.onProgress = (size_t dlTotal, size_t dlNow, size_t ulTotal, size_t ulNow) {
            emit(DownloadProgressEvent(dlTotal, dlNow));
            return 0;
        };

        m_client.perform();
        zipFile.close();

        auto zip = new ZipArchive(read(zipFileName));

        foreach (name, ArchiveMember am; zip.directory())
        {
            zip.expand(am);
            File(name, "w+").rawWrite(am.expandedData());
        }

        if (false == m_isGMInitialized)
        {

            version (GMagick_Dynamic) {
                void* libgm;
                loadGraphicsMagick(libgm);

                if (null is libgm) {
                    throw new PixivMagickException("No GraphicsMagick library found.");
                }
            }

            InitializeMagick(null);
            m_isGMInitialized = true;
        }

        Image* currentImage;
        Image* imageList;

        ImageInfo* imageInfo;
        ExceptionInfo exception;

        GetExceptionInfo(&exception);
        imageInfo = CloneImageInfo(null);
        imageList = NewImageList();

        foreach (frame; ugoira.getFrames())
        {        
            string filename = buildPath(getcwd(), frame.file);
            immutable char *cImageFilename = toStringz(filename);
            size_t length = strlen(cImageFilename);
            strncpy(imageInfo.filename.ptr, cImageFilename, length);

            currentImage = ReadImage(imageInfo, &exception);

            if (UndefinedException != exception.severity)
            {
                CatchException(&exception);
                string msg = format!"Error reading GIF Image: %s -- %s"(
                    fromStringz(exception.reason),
                    fromStringz(exception.description));
                throw new PixivException(msg);
            }

            if (null !is currentImage)
            {
                currentImage.delay = frame.delay / 10;
                currentImage.dispose = PreviousDispose;
                currentImage.iterations = 0;
                AppendImageToList(&imageList, currentImage);
            }
        }

        if (null !is imageList)
        {
            imageInfo.adjoin = MagickTrue;

            chdir(directory);
            immutable char* cGIFFilename = toStringz(gifFileName);
            WriteImages(imageInfo, imageList, cGIFFilename, &exception);

            if (UndefinedException != exception.severity)
            {
                CatchException(&exception);
                string msg = format!"Error reading GIF Image: %s -- %s"(
                    fromStringz(exception.reason),
                    fromStringz(exception.description));
                throw new PixivException(msg);
            }
            DestroyImageList(imageList);
        }

        if (null !is imageInfo)
        {
            DestroyImageInfo(imageInfo);
        }

        DestroyExceptionInfo(&exception);
        emit(DownloadCompleteEvent());
    }

    void _resetHeaders()
    {
        m_client.clearRequestHeaders();

        m_client.addRequestHeader("Accept", "application/json, */*");
        m_client.addRequestHeader("Host", "www.pixiv.net");
        m_client.addRequestHeader("Referer", "https://www.pixiv.net/");
        m_client.setUserAgent(kUserAgent);
        if ("" != m_phpsessid)
        {
            m_client.setCookie("PHPSESSID=" ~ m_phpsessid);
        }
    }

    ///
    /// Setup m_client for resuming a previous download.
    ///
    /// If the particular request doesn't support partial
    /// downloads
    void _setupPartialDownload(string url, string filename)
    {
        import std.file : getSize;

        m_client.url = url;
        m_client.method = HTTP.Method.head;
        m_client.perform();
        if ("accept-ranges" in m_client.responseHeaders()) {
            ulong bytesRead = getSize(filename);
            m_client.addRequestHeader("Range", format!"bytes=%d-"(bytesRead));
        }
        // If the "accept-ranges" header was not present,
        // we'll restart the entire download.
        m_client.method = HTTP.Method.get;
    }
}
