module pixivd.types.thumbnail;

import std.json;

import pixivd.enums : ContentType;
import pixivd.mixins;

class Thumbnail
{
    string id;
    string title;
    ContentType type;
    long xRestrict;
    long restrict;
    long sl;
    string url;
    string description;
    string[] tags;
    string userId;
    string userName;
    long width;
    long height;
    long pages;
    bool canBookmark;
    // bookmarkData;
    string alt;
    // titleCaptionTranslation
    string createDate;
    string updateDate;
    bool isUnlisted;
    bool isMasked;
    string[string] urls;
    string profileImageUrl;

    static Thumbnail fromJSON(const ref JSONValue json)
    {
        auto thumb = new Thumbnail();

        with (thumb)
        {
            id = json["id"].str;
            title = json["title"].str;
            type = cast(ContentType) json["illustType"].integer;
            xRestrict = json["xRestrict"].integer;
            restrict = json["restrict"].integer;
            sl = json["sl"].integer;
            url = json["url"].str;
            description = json["description"].str;

            auto ptags = json["tags"].array;
            foreach (ref tag; ptags)
            {
                tags ~= tag.str;
            }

            userId = json["userId"].str;
            userName = json["userName"].str;
            width = json["width"].integer;
            height = json["height"].integer;
            pages = json["pageCount"].integer;
            canBookmark = mixin(mixGetJsonBoolean!("json", "isBookmarkable"));
            // bookmarkData
            alt = json["alt"].str;
            // titleCaptionTranslation
            createDate = json["createDate"].str;
            updateDate = json["updateDate"].str;
            isUnlisted = mixin(mixGetJsonBoolean!("json", "isUnlisted"));
            isMasked = mixin(mixGetJsonBoolean!("json", "isMasked"));

            auto purls = json["urls"];
            foreach(ref size; ["250x250", "360x360", "540x540"])
            {
                urls[size] = purls[size].str;
            }

            profileImageUrl = json["profileImageUrl"].str;
        }

        return thumb;
    }
}
