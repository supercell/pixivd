module pixivd.types.user;

import std.json;

import pixivd.mixins;

class User
{
public:
    bool acceptRequest;
    bool following;
    bool followed;
    // illusts;
    bool isBlocking;
    bool isMypixiv;
    // novels;
    string profileImageUrl;
    string userComment;
    string userId;
    string userName;

    static User fromJSON(const ref JSONValue json)
    {
        auto user = new User();

        with (user) {
            acceptRequest = mixin(mixGetJsonBoolean!("json", "acceptRequest"));
            following = mixin(mixGetJsonBoolean!("json", "following"));
            followed = mixin(mixGetJsonBoolean!("json", "followed"));
            isBlocking = mixin(mixGetJsonBoolean!("json", "isBlocking"));
            isMypixiv = mixin(mixGetJsonBoolean!("json", "isMypixiv"));

            profileImageUrl = json["profileImageUrl"].str;
            userComment = json["userComment"].str;
            userId = json["userId"].str;
            userName = json["userName"].str;
        }

        return user;
    }
}

class FullUser : User
{
    public string profileImageUrlBig;

    static FullUser fromJSON(const ref JSONValue json)
    {
        auto user = new FullUser();

        with (user)
        {
            userId = json["userId"].str;
            userName = json["name"].str;
            profileImageUrl = json["image"].str;
            profileImageUrlBig = json["imageBig"].str;

            following = mixin(mixGetJsonBoolean!("json", "isFollowed"));
            followed = mixin(mixGetJsonBoolean!("json", "followedBack"));
        }

        return user;
    }
}

/**
Information about a user and their illustrations, manga, novels, etc.
*/
class UserBrief
{
public:
    struct Bookmark
    {
        long illust;
        long novel;
    }

    struct SiteWorksStatus
    {
        bool booth;
        bool sketch;
        bool vroidHub;
    }

public:
    Bookmark[string] bookmarkCount;
    string[] illusts;
    string[] manga;
    // ?[] novels;
    // ?[] mangaSeries;
    // ?[] novelsSeries;
    // Illustration[](?) pickup;
    // ? request;

    static UserBrief fromJSON(const ref JSONValue json)
    {
        auto ub = new UserBrief();

        static if (__VERSION__ < 2083L) {
            JSON_TYPE arrayType = JSON_TYPE.ARRAY;
        } else {
            JSONType arrayType = JSONType.array;
        }
        
        with(ub) {
            bookmarkCount["public"] = Bookmark(
                json["bookmarkCount"]["public"]["illust"].integer,
                json["bookmarkCount"]["public"]["novel"].integer
            );
            bookmarkCount["private"] = Bookmark(
                json["bookmarkCount"]["private"]["illust"].integer,
                json["bookmarkCount"]["private"]["novel"].integer
            );
            
            /* 
             * When an account doesn't have any illustrations or manga
             * then the return type is an empty array, however, if there
             * are illustrations or manga, then the type is an JSONObject.
             */

            if ("illusts" in json && (arrayType != json["illusts"].type)) {
                foreach(k, v; json["illusts"].object) {
                    illusts ~= k;
                }
            }
            
            if ("manga" in json && (arrayType != json["manga"].type)) {
                foreach(k, v; json["manga"].object) {
                    manga ~= k;
                }
            }
        }
        
        return ub;
    }
}
