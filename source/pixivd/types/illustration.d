module pixivd.types.illustration;

import std.json;

import pixivd.types.pixiv_exception;
import pixivd.enums : ContentType;
import pixivd.mixins;

class Illustration
{
    string id;
    string title;
    string description;
    ContentType type;
    string createDate;
    string uploadDate;
    size_t restrict;
    size_t xRestrict;
    string[string] urls;
    // tags
    string alt;
    string userId;
    string userName;
    string userAccount;
    // userIllusts
    bool likeData;
    size_t width;
    size_t height;
    size_t pages;
    size_t bookmarks;
    size_t likes;
    size_t comments;
    size_t responses;
    size_t views;

    /**
     * Create an Illustration from a JSONValue.
     */
    static Illustration fromJSON(const ref JSONValue json)
    {
        if (mixin(mixCheckJsonError!("json"))) {
            throw new PixivJSONException(json["message"].str);
        }

        if ("body" !in json) {
            throw new PixivJSONException("No \"body\" in returned JSON.");
        }

        JSONValue body_ = json["body"];
        auto illust = new Illustration();

        // Set the image properties.
        // Nested in `try` because any of the .str/.integer/.etc methods
        // can throw if the JSON value doesn't match the type.
        try {
            illust.id = body_["id"].str;
            illust.title = body_["title"].str;
            illust.description = body_["description"].str;
            illust.type = cast(ContentType)body_["illustType"].integer;
            illust.createDate = body_["createDate"].str;
            illust.uploadDate = body_["uploadDate"].str;
            illust.restrict = body_["restrict"].integer;
            illust.xRestrict = body_["xRestrict"].integer;

            auto urls = body_["urls"];

            illust.urls["mini"] = urls["mini"].str;
            illust.urls["thumb"] = urls["thumb"].str;
            illust.urls["small"] = urls["small"].str;
            illust.urls["regular"] = urls["regular"].str;
            illust.urls["original"] = urls["original"].str;

            illust.alt = body_["alt"].str;
            illust.userId = body_["userId"].str;
            illust.userName = body_["userName"].str;
            illust.userAccount = body_["userAccount"].str;

            // userIllusts

            illust.likeData = mixin(mixGetJsonBoolean!("body_", "likeData"));
            illust.width = body_["width"].integer;
            illust.height = body_["height"].integer;
            illust.pages = body_["pageCount"].integer;
            illust.bookmarks = body_["bookmarkCount"].integer;
            illust.likes = body_["likeCount"].integer;
            illust.comments = body_["commentCount"].integer;
            illust.responses = body_["responseCount"].integer;
            illust.views = body_["viewCount"].integer;

            return illust;
        } catch (JSONException e) {
            throw new PixivJSONException(e.msg);
        }
    }
}
