module pixivd.types.ugoira;

import std.json;

import pixivd.mixins;
import pixivd.types.pixiv_exception : PixivJSONException;

class Ugoira
{
public:

    struct Frame {
        immutable string file;
        immutable long delay;
    }

private:
    string mimeType;
    string originalSource;
    string source;
    Frame[] frames;

public:
    static Ugoira fromJSON(const ref JSONValue json) {

        if (mixin(mixCheckJsonError!("json"))) {
            throw new PixivJSONException(json["message"].str);
        }

        static if (__VERSION__ < 2083L) {
            JSON_TYPE arrayType = JSON_TYPE.ARRAY;
        } else {
            JSONType arrayType = JSONType.array;
        }

        auto bdy = json["body"];
        auto ugoira = new Ugoira();

        with (ugoira) {
            mimeType = bdy["mime_type"].str;
            originalSource = bdy["originalSrc"].str;
            source = bdy["src"].str;

            auto jsonframes = bdy["frames"].array;
            foreach (const ref JSONValue frame; jsonframes) {
                ugoira.frames ~= Frame(frame["file"].str, frame["delay"].integer);
            }
        }

        return ugoira;
    }

    string getMimeType() const
    {
        return mimeType;
    }

    Frame[] getFrames()
    {
        return frames;
    }

    string getOriginalSource() const
    {
        return originalSource;
    }

    string getSource() const
    {
        return source;
    }
}
