module pixivd.types.pixiv_exception;

class PixivException : Exception
{
    this(string msg)
    {
        super(msg);
    }
}

/// Currently only used in downloadIllust to notify
/// that Dynamically loading GraphicsMagick returned null.
class PixivMagickException : PixivException
{
    this(string msg)
    {
        super(msg);
    }
}

/// Exception with the JSON response.
class PixivJSONException : PixivException
{
    this(string msg)
    {
        super(msg);
    }
}
