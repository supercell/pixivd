/+ dub.sdl:
    name "download_pixiv_image"
    dependency "pixivd" path="../"
 +/

/*
 * If you have dub (http://dub.pm) installed, you can run this file with:
 *   dub --single examples/download_pixiv_image.d -- $ID1 $ID2 $ID3...
 */

import std.file : getcwd;
import std.stdio;
import std.string : strip;

/*
 * pixivd makes no attempt to prevent an "anti-robot" page (i.e. CloudFlare)
 * from interrupting your download. You should do this yourself.
 */
import core.thread : Thread;
import core.time : seconds;
import std.random : uniform;

import pixivd;
import pixivd.types;

int main(string[] args)
{
    if (args.length < 2) {
        stderr.writefln("error: missing illustration id");
        stderr.writefln("usage: %s <id...>", args[0]);
        return 1;
    }
    
    write("enter your PHPSESSID: ");
    scope sessionID = readln().strip();
    
    auto client = new Client(sessionID);

    foreach (index, string id; args[1..$]) {
        scope Illustration illust = client.fetchIllustration(id);
        writefln("Downloading illustration %s (%s)...", illust.id, illust.title);
        client.downloadIllust(illust, getcwd(), /* overwrite = */ true);
        
        if (index + 1 < args[1..$].length) {
            uint sleepDuration = uniform(2, 5);
            writefln("Sleeping for %d seconds...", sleepDuration);
            Thread.sleep(sleepDuration.seconds);
        }
    }

    return 0;
}
