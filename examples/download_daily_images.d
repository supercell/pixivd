/+ dub.sdl:
    name "download_daily_images"
    dependency "pixivd" path="../"
 +/

import std.conv : to;
import std.file : exists, getcwd, mkdir;
import std.path : buildPath;
import std.stdio;
import std.string : strip;

/*
 * pixivd doesn't attempt to prevent "anti-robot" measures.
 * You need to do this yourself.  The below is just a very basic (and slow)
 * method.
 */
import core.thread : Thread;
import core.time : seconds;
import std.random : uniform;

import pixivd;
import pixivd.types;

int main(string[] args)
{
    write("Please enter your PHPSESSID: ");
    string sessionId = readln().strip();
    
    auto client = new Client(sessionId);
    
    /* fetch the first page of daily illustrations */
    Thumbnail[] thumbnails = client.fetchIllustrationsFollowing();
    
    foreach (index, Thumbnail thumb; thumbnails) {
        writefln("Downloading illustration id %d...", thumb.id);
        /* fetch the actual content piece */
        scope Illustration illust = client.fetchIllustration(thumb.id);
        string artistDir = buildPath(getcwd(),
            illust.userId ~ "_" ~ illust.userName);
        
        if (false == exists(artistDir))
            mkdir(artistDir);

        client.downloadIllust(illust, artistDir);
        
        if (index + 1 < thumbnails.length) {
            /* So we don't spam the server too much. */
            int sleepDuration = uniform(3, 8);
            writefln("Sleeping for %d seconds...", sleepDuration);
            Thread.sleep(sleepDuration.seconds);
        }
    }

    return 0;
}
