#!/bin/bash
# -----------------------------------------------------------------------------
#
# Copyright (C) 2022 mio <stigma@disroot.org>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# -----------------------------------------------------------------------------

FALSE=0
TRUE=1

test -d ../magickd && test -f ../magickd/graphicsmagick_c/dub.sdl && {
    printf "magickd already downloaded :)\n"
    exit 0
}

strlen() {
    if [ $# -lt 1 ]
    then
        return 1
    else
        printf "$1" | wc -m
        return 0
    fi
}

PIXIVD_DIR="$(pwd)"
PARENT_DIR="$(dirname $PIXIVD_DIR)"

printf "*********************************************************\n"
printf "*                        WARNING                        *\n"
printf "*********************************************************\n\n"
printf " This script will create a new directory named 'magickd'\n"
printf " in $PARENT_DIR\n\n"
read -p " Do you want to continue? [yes/(no)]: " WILL_PROCEED

if [[ $WILL_PROCEED != [yY] && $WILL_PROCEED != [yY][eE][sS] ]]
then
    printf "\nNot proceeding with downloading magickd.\n"
    exit 0
fi

printf "\n"

_has_git=$FALSE

which git > /dev/null && _has_git=$TRUE

cd ../

if [ $TRUE -eq $_has_git ]
then
    printf "Cloning magickd via git...\n"

    # In future versions, this would then checkout the appropriate version,
    # but since pixivd uses the lastest commit, this doesn't matter.
    git clone "https://repo.or.cz/magickd.git"

    if [ $? -ne 0 ]
    then
        printf "Failed to clone magickd.\n"
        exit 1
    fi

    printf "Successfully cloned magicked!\n"

    exit 0
else
    _has_wget=$FALSE
    _has_curl=$FALSE
    _has_gzip=$FALSE
    _tar_supports_gzip=$FALSE

    which wget > /dev/null && _has_wget=$TRUE
    which curl > /dev/null && _has_curl=$TRUE

    which gzip > /dev/null && _has_gzip=$TRUE
    tar --help | grep '\--gzip' > /dev/null && _tar_supports_gzip=$TRUE

    # Similar to the git clone, pixivd uses the lastest commit,
    # so no need for version information yet.
    url="https://repo.or.cz/magickd.git/snapshot/master.tar.gz"

    if [ $TRUE -eq $_has_wget ]
    then
        printf "Downloading magickd via wget...\n"
        wget --no-verbose --show-progress --output-document "magickd.tar.gz" \
            "$url"
    elif [ $TRUE -eq $_has_curl ]
    then
        printf "Downloading magickd via curl...\n"
        curl --output "magickd.tar.gz" "$url"
    else
        printf "Unable to download magickd automatically.\n" >&2
        printf "Please visit %s and extract in the parent directory of %s.\n" \
            "$url" "$PIXIVD_DIR"
        exit 1
    fi

    if [ $TRUE -eq $_has_gzip ]
    then
        printf "Extracting magickd.tar.gz...\n"
        sleep 2
        gzip --decompress --to-stdout --verbose "magickd.tar.gz" | \
            tar --extract --verbose
    elif [ $TRUE -eq $_tar_supports_gzip ]
    then
        printf "Extracting magickd.tar.gz...\n"
        sleep 2
        tar --extract --gzip --verbose < magickd.tar.gz
    else
        printf "gzip (%s) not installed, and tar doesn't support gzip.\n" \
            "https://www.gnu.org/software/gzip" >&2
        exit 1
    fi

    printf "Successfully downloaded and extracted magickd!\n%s\n" \
        "Make sure to change the dub.sdl file to use a local path."
fi
